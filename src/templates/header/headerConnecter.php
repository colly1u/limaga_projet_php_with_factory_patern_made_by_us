<html>
<head>
    <title>Limaga</title>

    <?php
    $app = \Slim\Slim::getInstance();
    $css = $app->request()->getRootUri().'/src/css/BootStrap.css';
    $img = $app->request()->getRootUri().'/src/img/home_icon.png';
    $deco= $app->request()->getRootUri().'/deconnexion';
    $magasin = $app->request()->getRootUri().'/magasin';
    $home = $app->request()->getRootUri().'/';
    $famille = $app->request->getRootUri().'/famille';
    $fact = $app->request->getRootUri().'/facture';
    ?>
    <link rel="stylesheet" type="text/css" href="<?php echo $css?>"/>
</head>
<body>
<header>
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="btn btn-link" href="<?php echo $home?>"><img src="<?php echo $img ?>"></a>
                <a class="btn btn-link" href="<?php echo $magasin?>">Magasin</a>
                <a class="btn btn-link" href="<?php echo $famille?>">Gestion Famille</a>
                <a class="btn btn-link" href="<?php echo $fact?>">Mes Factures</a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="<?php echo $deco ?>">Deconnexion</a></li>
                </ul>
            </div>

    </nav>

    <center><h1>Limaga</h1></center>
</header>