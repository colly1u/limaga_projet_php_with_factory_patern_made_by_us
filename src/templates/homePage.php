<html>
<head>
    <title>Limaga</title>
    <link rel="stylesheet" type="text/css" href="BootStrap.css">
    <?php
    $app = \Slim\Slim::getInstance();
    $css = $app->request()->getRootUri().'/src/css/BootStrap.css';
    ?>
    <link rel="stylesheet" type="text/css" href="<?php echo $css?>"/>
</head>
<body>
<center><h1>Limaga</h1></center>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3">

                <center> <img src="src/img/head.png"></center>

                <div style="padding-top:20px;">
                    <center>
                        <button type="reset" class="btn btn-default" onclick="self.location.href='formInscription'">Inscription</button>
                        <button type="submit" class="btn btn-primary" onclick="self.location.href='login'">Connexion</button>
                    </center>
                </div>
            </div>
        </div>
</section>

</body>
</html>
