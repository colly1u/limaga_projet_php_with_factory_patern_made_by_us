<?php
$app = \Slim\Slim::getInstance();
$javascript = $app->request()->getRootUri().'/src/javascript/onglet.js';
?>
<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <ul class="nav nav-tabs">
                    <li class="" id="onglet_entree" onclick="change_onglet('entree')"><a>Entree</a></li>
                    <li class="" id="onglet_entreeF" onclick="change_onglet('entreeF')"><a>Entree Famille</a></li>
                    <li class="" id="onglet_abonnement" onclick="change_onglet('abonnement')"><a>Abonnement</a></li>
                    <li class="" id="onglet_abonnementF" onclick="change_onglet('abonnementF')"><a>Abonnement Famille</a></li>
                    <li class="" id="onglet_prodDeriv" onclick="change_onglet('prodDeriv')"><a>Produits Derives</a></li>
                </ul>
                <div id="myTabContent" class="tab-content">
                    <div class="tab-pane fade" id="contenu_onglet_entree">
                        <p><?php $vue=new \src\limagaapp\views\VuePrincipal();
                            $vue->formbillet();
                               ?></p>
                    </div>
                    <div class="tab-pane fade" id="contenu_onglet_entreeF">
                        <p><?php $vue=new \src\limagaapp\views\VuePrincipal();
                            $vue->formBilletFamille();
                            ?></p>
                    </div>
                    <div class="tab-pane fade" id="contenu_onglet_abonnement">
                        <p><?php $vue=new \src\limagaapp\views\VuePrincipal();
                            $vue->formAbonnement();
                            ?></p>
                    </div>
                    <div class="tab-pane fade" id="contenu_onglet_abonnementF">
                        <p><?php $vue=new \src\limagaapp\views\VuePrincipal();
                            $vue->formAbonnementFamille();
                            ?></p>
                    </div>
                    <div class="tab-pane fade" id="contenu_onglet_prodDeriv">
                        <p>Pour ceux qui veulent aller a la piscine mais qui n'ont pas de matos</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type= "text/javascript" src="<?php echo $javascript?>">

    </script>
</section>