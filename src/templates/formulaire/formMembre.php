<div class="container">
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <form class="form-horizontal" role="form" method="post" action="formMembreValid">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <center><h2 class="panel-title">Ajouter un Membre</h2></center>
                    </div>
                    <div class="panel-body">




                        <label for="inputNom" class="col-lg-1 control-label">Nom</label>
                        <div class="col-lg-3">
                            <input required="required" type="text" class="form-control" name="inputNom"
                                   placeholder="Nom">
                        </div>

                        <label for="inputPrenom" class="col-lg-1 control-label">Prenom</label>
                        <div class="col-lg-3">
                            <input required="required" type="text" class="form-control" name="inputPrenom"
                                   placeholder="Prenom">
                        </div>

                        <div class="form-group">
                            <label for="inputAge" class="col-lg-1 control-label">Age</label>
                            <div class="col-lg-3">
                                <input required="required" type="number" class="form-control" name="inputAge"
                                       placeholder="Age">
                            </div>

                        </div>

                        <div class="form-group">
                            <label for="select" class="col-lg-3 control-label">Niveau
                                natation</label>
                            <div class="col-lg-8 col-lg-offset">
                                <select class="form-control" name="selectLvlNatation">
                                    <option value="Debutant">Debutant</option>
                                    <option value="Intermediaire">Intermediaire</option>
                                    <option value="Bon">Bon nageur</option>
                                    <option value="Tres bon">Tres bon nageur</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-5 col-lg-offset-5">
                            <button value="ok" name="valider" type="submit" class="btn btn-primary"> Ajouter membre</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>


