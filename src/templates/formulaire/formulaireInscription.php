
<section>
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-lg-offset-3">
				<form class="form-horizontal" role="form" method="post" action="formInscriptionValid">
					<fieldset>
						<legend>Inscription</legend>

						<div class="form-group">
							<label for="inputEmail" class="col-lg-2 control-label">Email</label>
							<div class="col-lg-10">
								<input required="required" type="email" class="form-control" name="inputEmail"
									placeholder="Email">
							</div>
						</div>

						<div class="form-group">
							<label for="inputPseudo" class="col-lg-2 control-label">Pseudo</label>
							<div class="col-lg-10">
								<input required="required" type="text" class="form-control" name="inputPseudo"
									placeholder="Pseudo">
							</div>
						</div>

						<div class="form-group">
							<label for="inputPassword" class="col-lg-2 control-label">Password</label>
							<div class="col-lg-10">
								<input required="required" type="password" class="form-control" name="inputPassword"
									placeholder="Password">
							</div>
						</div>

						<div class="form-group">
							<label for="inputNom" class="col-lg-2 control-label">Nom</label>
							<div class="col-lg-10">
								<input required="required" type="text" class="form-control" name="inputNom"
									placeholder="Nom">
							</div>
						</div>


						<div class="form-group">
							<label for="inputPrenom" class="col-lg-2 control-label">Prenom</label>
							<div class="col-lg-10">
								<input required="required" type="text" class="form-control" name="inputPrenom"
									placeholder="Prenom">
							</div>
						</div>


						<div class="form-group">
							<label for="inputAdresse" class="col-lg-2 control-label">Adresse</label>
							<div class="col-lg-10">
								<input required="required" type="text" class="form-control"name="inputAdresse"
									placeholder="Adresse">
							</div>
						</div>
							
						<div class="form-group">
							<label for="inputAge" class="col-lg-2 control-label">Age</label>
							<div class="col-lg-10">
								<input required="required" type="number" class="form-control" name="inputAge"
									placeholder="Age">
							</div>
						</div>
						

						<div class="form-group">
							<label for="inputDate" class="col-lg-3 control-label">date de
								naissance</label>
							<div class="col-lg-3 col-lg-offset-1">
								<input required="required" type="number" class="form-control" name="inputDateD"
									placeholder="Ex: 4">
							</div>
							<div class="col-lg-2">
								<input required="required" type="number" class="form-control" name="inputDateM"
									placeholder="Ex:8 ">
							</div>
							<div class="col-lg-3">
								<input required="required" type="number" class="form-control" name="inputDateY"
									placeholder="Ex:1994">
							</div>
						</div>



						<div class="form-group">
							<label for="inputTel" class="col-lg-4 control-label">Numero
								de telephone </label>
							<div class="col-lg-8">
								<input required="required" type="text" class="form-control" name="inputTel"
									placeholder="06(ou 07).xx.xx.xx.xx">
							</div>
						</div>






						<div class="form-group">
							<label for="select" class="col-lg-4 control-label">Niveau
								natation</label>
							<div class="col-lg-8">
								<select class="form-control" name="selectLvlNatation">
									<option value="Debutant">Debutant</option>
									<option value="Intermediaire">Intermediaire</option>
									<option value="Bon">Bon nageur</option>
									<option value="Tres bon">Tres bon nageur</option>
								</select> <br>

							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-6 col-lg-offset-4">
								<button type="reset" class="btn btn-default"
									onclick="self.location.href='#'">Annuler</button>
								<button type="submit" name="valider" value="ok" class="btn btn-primary">Valider</button>
							</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>

</section>