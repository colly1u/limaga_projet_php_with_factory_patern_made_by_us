<form class="form-horizontal" role="form" method="post" action="formAbonnementFamilleValid">
    <div class="panel panel-success">
        <div class="panel-heading">
            <center><h2 class="panel-title">Acheter un e-abonnement famille </h2></center>
        </div>
        <div class="panel-body">

            <div class="form-group">
                <label for="inputDate" class="col-lg-3 control-label">nombre d entree souhaiter</label>
                <div class="col-lg-3 col-lg-offset-1">
                    <input required="required" type="number" class="form-control" name="inputEntree"
                           placeholder="Ex: 4">
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-lg-5 col-lg-offset-5">
                <button type="submit" value="ok" name="valider" class="btn btn-primary"> Ajouter au panier</button>
            </div>
        </div>
    </div>
</form>