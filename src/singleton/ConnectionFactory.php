<?php
/**
 * Created by PhpStorm.
 * User: Florian
 * Date: 30/04/2015
 * Time: 15:19
 */
namespace src\singleton;
use Illuminate\Database\Capsule\Manager as DB;

class ConnectionFactory {
   private static $connection;


    public static function getConnection($filename) {
        if (!self::$connection) {
            $connection=new DB();
            $config=parse_ini_file($filename);
            $connection->addConnection($config);
            $connection->setAsGlobal();
            $connection->bootEloquent();
        }
        return self::$connection;
    }
}