/**
 * Created by Pierre on 11/06/2015.
 */
var anc_onglet;

function premierAffichage(){
    anc_onglet = 'entree';
    document.getElementById('onglet_entree').className = 'active';
    document.getElementById('contenu_onglet_entree').className = 'tab-pane fade active in';
}

function change_onglet(name) {
    document.getElementById('onglet_' + anc_onglet).className = '';
    document.getElementById('onglet_' + name).className = 'active';
    document.getElementById('contenu_onglet_' + anc_onglet).className = 'tab-pane fade';
    document.getElementById('contenu_onglet_' + name).className = 'tab-pane fade active in';
    anc_onglet = name;
}

window.onload = premierAffichage();