-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Ven 12 Juin 2015 à 22:24
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `limagadb`
--

-- --------------------------------------------------------

--
-- Structure de la table `boncommande`
--

CREATE TABLE IF NOT EXISTS `boncommande` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `client_id` int(10) NOT NULL,
  `date_commande` date NOT NULL,
  `Total` double(6,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `client_id` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE IF NOT EXISTS `client` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `prenom` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `adresse` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `numTel` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `dateNais` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `age` int(4) NOT NULL,
  `lvl_natation` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Contenu de la table `client`
--

INSERT INTO `client` (`id`, `nom`, `prenom`, `adresse`, `numTel`, `dateNais`, `age`, `lvl_natation`) VALUES
(7, 'Colly', 'Florian', '49 rue de Lawou', '0648512438', '04/08/1994', 18, 'Intermediaire'),
(8, 'dsf', 'fsfs', 'dqsfs', '0648512438', '01/04/1994', 18, 'Debutant'),
(9, 'dsf', 'fsfs', 'dqsfs', '0648512438', '01/04/1994', 18, 'Intermediaire'),
(10, 'cwcw', 'sdfsf', 'dqfq', '0648512438', '04/08/1992', 19, 'Bon'),
(11, 'dDd', 'fqsdqsd', 'fqsdq', '0648512438', '01/05/1994', 18, 'Intermediaire');

-- --------------------------------------------------------

--
-- Structure de la table `e_abonnement`
--

CREATE TABLE IF NOT EXISTS `e_abonnement` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `client_id` int(10) NOT NULL,
  `produit_id` int(10) NOT NULL,
  `nb_billet` int(4) NOT NULL,
  `codeBarre` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_client` (`client_id`),
  KEY `produit_id` (`produit_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `e_abonnement`
--

INSERT INTO `e_abonnement` (`id`, `client_id`, `produit_id`, `nb_billet`, `codeBarre`) VALUES
(3, 7, 5, 4, '71');

-- --------------------------------------------------------

--
-- Structure de la table `e_abonnementfamille`
--

CREATE TABLE IF NOT EXISTS `e_abonnementfamille` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `client_id` int(10) NOT NULL,
  `produit_id` int(10) NOT NULL,
  `nb_billet` int(10) NOT NULL,
  `codeBarre` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `client_id` (`client_id`),
  KEY `produit_id` (`produit_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Contenu de la table `e_abonnementfamille`
--

INSERT INTO `e_abonnementfamille` (`id`, `client_id`, `produit_id`, `nb_billet`, `codeBarre`) VALUES
(1, 7, 4, 4, '71'),
(2, 7, 4, 4, '71'),
(3, 7, 4, 4, '71'),
(4, 7, 4, 4, '71'),
(5, 7, 4, 4, '71'),
(6, 7, 4, 6, '71'),
(7, 7, 4, 6, '71'),
(8, 7, 4, 4, '71');

-- --------------------------------------------------------

--
-- Structure de la table `e_billet`
--

CREATE TABLE IF NOT EXISTS `e_billet` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `client_id` int(10) NOT NULL,
  `produit_id` int(10) NOT NULL,
  `jour` int(3) NOT NULL,
  `mois` int(3) NOT NULL,
  `annee` int(5) NOT NULL,
  `durée` varchar(100) NOT NULL,
  `codeBarre` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_client` (`client_id`),
  KEY `produit_id` (`produit_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Contenu de la table `e_billet`
--

INSERT INTO `e_billet` (`id`, `client_id`, `produit_id`, `jour`, `mois`, `annee`, `durée`, `codeBarre`) VALUES
(1, 7, 1, 6, 11, 2015, 'matin', '16112015'),
(2, 7, 1, 8, 8, 2015, '', '71882015'),
(3, 7, 1, 8, 8, 2015, '', '71882015'),
(4, 7, 1, 8, 8, 2015, '', '71882015'),
(5, 7, 1, 8, 8, 2015, '', '71882015'),
(6, 7, 1, 8, 8, 2015, '', '71882015'),
(7, 7, 1, 8, 8, 2015, '', '71882015'),
(8, 7, 1, 8, 8, 2015, '', '71882015'),
(9, 7, 1, 8, 8, 2015, '', '71882015'),
(10, 7, 1, 8, 8, 2015, '', '71882015'),
(11, 7, 1, 8, 8, 2015, '', '71882015'),
(12, 7, 1, 8, 8, 2015, '', '71882015'),
(13, 7, 1, 8, 8, 2015, '', '71882015'),
(14, 7, 1, 8, 8, 2015, '', '71882015'),
(15, 7, 1, 8, 8, 2015, '', '71882015'),
(16, 7, 1, 4, 8, 1994, '', '71481994'),
(17, 7, 1, 4, 8, 1994, '', '71481994'),
(18, 7, 1, 4, 8, 1994, '', '71481994'),
(19, 7, 1, 4, 8, 1994, '', '71481994'),
(20, 7, 1, 4, 8, 1994, '', '71481994'),
(21, 7, 1, 4, 8, 2015, '', '71482015'),
(22, 7, 1, 5, 5, 2015, '', '71552015'),
(23, 7, 1, 4, 5, 2015, '', '71452015'),
(24, 7, 1, 4, 5, 2015, '', '71452015'),
(25, 7, 1, 4, 4, 2015, '', '71442015');

-- --------------------------------------------------------

--
-- Structure de la table `e_billetfamille`
--

CREATE TABLE IF NOT EXISTS `e_billetfamille` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `client_id` int(10) NOT NULL,
  `produit_id` int(10) NOT NULL,
  `jour` int(10) NOT NULL,
  `mois` int(3) NOT NULL,
  `annee` int(5) NOT NULL,
  `duree` varchar(100) NOT NULL,
  `codeBarre` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `client_id` (`client_id`),
  KEY `produit_id` (`produit_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Contenu de la table `e_billetfamille`
--

INSERT INTO `e_billetfamille` (`id`, `client_id`, `produit_id`, `jour`, `mois`, `annee`, `duree`, `codeBarre`) VALUES
(2, 7, 2, 4, 5, 2015, '', '71452015'),
(3, 7, 2, 4, 5, 2015, '', '71452015'),
(4, 7, 2, 4, 5, 2015, '', '71452015'),
(5, 7, 2, 4, 5, 2015, '', '71452015'),
(6, 7, 2, 4, 5, 2015, '', '71452015'),
(7, 7, 2, 4, 5, 2015, '', '71452015'),
(8, 7, 2, 4, 5, 2015, '', '71452015'),
(9, 7, 2, 4, 5, 2015, '', '71452015'),
(10, 7, 2, 4, 5, 2015, '', '71452015'),
(11, 7, 2, 4, 5, 2015, '', '71452015'),
(12, 7, 2, 4, 10, 2015, '', '714102015');

-- --------------------------------------------------------

--
-- Structure de la table `facture`
--

CREATE TABLE IF NOT EXISTS `facture` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `client_id` int(10) NOT NULL,
  `status` varchar(100) NOT NULL,
  `date_fact` date NOT NULL,
  `total` double(6,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `client_id` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `lettrerelance`
--

CREATE TABLE IF NOT EXISTS `lettrerelance` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `client_id` int(10) NOT NULL,
  `releve_id` int(10) NOT NULL,
  `date_lettre` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `client_id` (`client_id`),
  KEY `releve_id` (`releve_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `membrefamille`
--

CREATE TABLE IF NOT EXISTS `membrefamille` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(20) NOT NULL,
  `age` int(4) NOT NULL,
  `lvl_natation` varchar(50) NOT NULL,
  `client_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_client` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `panier`
--

CREATE TABLE IF NOT EXISTS `panier` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `client_id` int(10) NOT NULL,
  `produit_id` int(10) NOT NULL,
  `actif` tinyint(1) NOT NULL,
  `facture_id` int(10) DEFAULT NULL,
  `boncommande_id` int(10) DEFAULT NULL,
  `e_billet_id` int(10) DEFAULT NULL,
  `e_abonnement_id` int(10) DEFAULT NULL,
  `e_billetfamille_id` int(10) DEFAULT NULL,
  `e_abonnementfamille_id` int(10) DEFAULT NULL,
  `qte` int(10) NOT NULL,
  `prix` double(6,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `client_id` (`produit_id`,`e_billet_id`,`e_abonnement_id`),
  KEY `e_billet_id` (`e_billet_id`),
  KEY `e_abonnement_id` (`e_abonnement_id`),
  KEY `produit_id` (`produit_id`),
  KEY `e_billetfamille` (`e_billetfamille_id`,`e_abonnementfamille_id`),
  KEY `e_abonnementfamille` (`e_abonnementfamille_id`),
  KEY `facture_id` (`facture_id`),
  KEY `boncommande_id` (`boncommande_id`),
  KEY `client_id_2` (`client_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Contenu de la table `panier`
--

INSERT INTO `panier` (`id`, `client_id`, `produit_id`, `actif`, `facture_id`, `boncommande_id`, `e_billet_id`, `e_abonnement_id`, `e_billetfamille_id`, `e_abonnementfamille_id`, `qte`, `prix`) VALUES
(2, 7, 1, 0, NULL, NULL, 16, NULL, NULL, NULL, 1, 5.50),
(3, 7, 1, 0, NULL, NULL, 17, NULL, NULL, NULL, 1, 5.50),
(4, 7, 1, 0, NULL, NULL, 18, NULL, NULL, NULL, 1, 5.50),
(5, 7, 1, 0, NULL, NULL, 19, NULL, NULL, NULL, 1, 5.50),
(6, 7, 1, 0, NULL, NULL, 20, NULL, NULL, NULL, 1, 5.50),
(7, 7, 1, 0, NULL, NULL, 21, NULL, NULL, NULL, 1, 5.50),
(8, 7, 1, 0, NULL, NULL, 22, NULL, NULL, NULL, 1, 5.50),
(9, 7, 1, 0, NULL, NULL, 23, NULL, NULL, NULL, 1, 5.50),
(11, 7, 2, 0, NULL, NULL, NULL, NULL, 2, NULL, 1, 5.00),
(12, 7, 2, 0, NULL, NULL, NULL, NULL, 3, NULL, 1, 5.00),
(13, 7, 2, 0, NULL, NULL, NULL, NULL, 4, NULL, 1, 5.00),
(14, 7, 2, 0, NULL, NULL, NULL, NULL, 5, NULL, 1, 5.00),
(15, 7, 2, 0, NULL, NULL, NULL, NULL, 6, NULL, 1, 5.00),
(16, 7, 2, 0, NULL, NULL, NULL, NULL, 7, NULL, 1, 5.00),
(17, 7, 2, 0, NULL, NULL, NULL, NULL, 8, NULL, 1, 5.00),
(18, 7, 2, 0, NULL, NULL, NULL, NULL, 9, NULL, 1, 5.00),
(19, 7, 2, 0, NULL, NULL, NULL, NULL, 10, NULL, 1, 5.00),
(20, 7, 2, 0, NULL, NULL, NULL, NULL, 11, NULL, 1, 5.00),
(21, 7, 1, 0, NULL, NULL, 24, NULL, NULL, NULL, 1, 5.50),
(22, 7, 2, 0, NULL, NULL, NULL, NULL, 12, NULL, 1, 5.00),
(23, 7, 1, 0, NULL, NULL, 25, NULL, NULL, NULL, 1, 5.50),
(25, 7, 5, 0, NULL, NULL, NULL, 3, NULL, NULL, 1, 20.00),
(26, 7, 4, 0, NULL, NULL, NULL, NULL, NULL, 1, 1, 20.00),
(27, 7, 4, 0, NULL, NULL, NULL, NULL, NULL, 2, 1, 20.00),
(28, 7, 4, 0, NULL, NULL, NULL, NULL, NULL, 3, 1, 20.00),
(29, 7, 4, 0, NULL, NULL, NULL, NULL, NULL, 4, 1, 20.00),
(30, 7, 4, 0, NULL, NULL, NULL, NULL, NULL, 5, 1, 20.00),
(31, 7, 4, 0, NULL, NULL, NULL, NULL, NULL, 6, 1, 30.00),
(32, 7, 4, 0, NULL, NULL, NULL, NULL, NULL, 7, 1, 24.00),
(33, 7, 4, 0, NULL, NULL, NULL, NULL, NULL, 8, 1, 16.00);

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

CREATE TABLE IF NOT EXISTS `produit` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `Description` varchar(500) NOT NULL,
  `prixHT` double(6,2) NOT NULL,
  `prixTTC` double(6,2) NOT NULL,
  `prixTVA` double(6,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `produit`
--

INSERT INTO `produit` (`id`, `Description`, `prixHT`, `prixTTC`, `prixTVA`) VALUES
(1, 'e_billet Individuelle', 4.40, 5.50, 1.10),
(2, 'e-billet Famille', 4.00, 5.00, 1.00),
(3, 'lesson', 10.00, 12.00, 2.00),
(4, 'abonnement Famille', 3.20, 4.00, 0.80),
(5, 'abonnement Individuel', 3.60, 4.50, 0.90);

-- --------------------------------------------------------

--
-- Structure de la table `releve`
--

CREATE TABLE IF NOT EXISTS `releve` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `client_id` int(10) NOT NULL,
  `date_releve` date NOT NULL,
  `total` double(6,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE IF NOT EXISTS `utilisateur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(10) NOT NULL,
  `pseudo` varchar(100) CHARACTER SET utf8 NOT NULL,
  `pass` varchar(300) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `alea` varchar(200) NOT NULL,
  `date_enregistrement` date NOT NULL,
  `IP_enregistrement` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pseudo` (`pseudo`),
  KEY `id_client` (`client_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Contenu de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `client_id`, `pseudo`, `pass`, `email`, `alea`, `date_enregistrement`, `IP_enregistrement`) VALUES
(6, 7, 'colly1u', '$2y$12$lBK2DAQi3MxPMSKto8ZQJ.bf7ufzR6FLO5MgM2WCAmwAV4/QCfqY2', 'mapo@hotmail.fr', '12', '2015-06-11', '::1'),
(7, 8, 'keke', '$2y$12$BrNri4VN.oJzsVeFYW.dDOZp8TsYy18xMV79jHXOwFGOY.cWlcSve', 'fdslfd@hotmail.fr', '12', '2015-06-12', '::1');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `e_abonnement`
--
ALTER TABLE `e_abonnement`
  ADD CONSTRAINT `e_abonnement/produit` FOREIGN KEY (`produit_id`) REFERENCES `produit` (`id`),
  ADD CONSTRAINT `proprietaire abo` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`);

--
-- Contraintes pour la table `e_abonnementfamille`
--
ALTER TABLE `e_abonnementfamille`
  ADD CONSTRAINT `produit/abo` FOREIGN KEY (`produit_id`) REFERENCES `produit` (`id`),
  ADD CONSTRAINT `client/e_abofamille` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`);

--
-- Contraintes pour la table `e_billet`
--
ALTER TABLE `e_billet`
  ADD CONSTRAINT `AtrributionBillet` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`),
  ADD CONSTRAINT `produit/e-billet` FOREIGN KEY (`produit_id`) REFERENCES `produit` (`id`);

--
-- Contraintes pour la table `e_billetfamille`
--
ALTER TABLE `e_billetfamille`
  ADD CONSTRAINT `e_billetfamille_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`),
  ADD CONSTRAINT `produit/e_billetFamille` FOREIGN KEY (`produit_id`) REFERENCES `produit` (`id`);

--
-- Contraintes pour la table `facture`
--
ALTER TABLE `facture`
  ADD CONSTRAINT `facture/client` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`);

--
-- Contraintes pour la table `membrefamille`
--
ALTER TABLE `membrefamille`
  ADD CONSTRAINT `lienFamiliale` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`);

--
-- Contraintes pour la table `panier`
--
ALTER TABLE `panier`
  ADD CONSTRAINT `panier_ibfk_10` FOREIGN KEY (`e_billetfamille_id`) REFERENCES `e_billetfamille` (`id`),
  ADD CONSTRAINT `panier_ibfk_2` FOREIGN KEY (`produit_id`) REFERENCES `produit` (`id`),
  ADD CONSTRAINT `panier_ibfk_3` FOREIGN KEY (`facture_id`) REFERENCES `facture` (`id`),
  ADD CONSTRAINT `panier_ibfk_4` FOREIGN KEY (`boncommande_id`) REFERENCES `boncommande` (`id`),
  ADD CONSTRAINT `panier_ibfk_5` FOREIGN KEY (`e_billet_id`) REFERENCES `e_billet` (`id`),
  ADD CONSTRAINT `panier_ibfk_6` FOREIGN KEY (`e_abonnement_id`) REFERENCES `e_abonnement` (`id`),
  ADD CONSTRAINT `panier_ibfk_8` FOREIGN KEY (`e_abonnementfamille_id`) REFERENCES `e_abonnementfamille` (`id`),
  ADD CONSTRAINT `panier_ibfk_9` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`);

--
-- Contraintes pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD CONSTRAINT `client/user` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
