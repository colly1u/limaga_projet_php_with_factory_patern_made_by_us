<?php
/**
 * Created by PhpStorm.
 * User: Florian
 * Date: 09/06/2015
 * Time: 14:10
 */
namespace src\limagaapp\model;
use Illuminate\Database\Eloquent\Model;

class Eabonnement extends Model {

    protected $table = 'e_abonnement';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function client (){
        return $this->belongsTo('Client');
    }

    public function Produit(){
        return $this->belongsTo('Produit');

    }




}