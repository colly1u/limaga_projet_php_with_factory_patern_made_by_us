<?php
/**
 * Created by PhpStorm.
 * User: Florian
 * Date: 11/06/2015
 * Time: 15:04
 */

namespace src\limagaapp\model;
use Illuminate\Database\Eloquent\Model;

class EabonnementFamille extends Model{

    protected $table = 'e_abonnementfamille';
    protected $primaryKey = 'id';
    public $timestamps = false;


    public function client (){
        return $this->belongsTo('Client');
    }

    public function Produit(){
        return $this->belongsTo('Produit');

    }



}