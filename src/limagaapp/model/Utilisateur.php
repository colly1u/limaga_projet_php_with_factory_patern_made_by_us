<?php
/**
 * Created by PhpStorm.
 * User: Florian
 * Date: 09/06/2015
 * Time: 09:40
 */

namespace src\limagaapp\model;
use Illuminate\Database\Eloquent\Model;

class Utilisateur extends Model {

    protected $table = 'utilisateur';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function client(){
        return $this->belongsTo('Client');

    }

}

