<?php
/**
 * Created by PhpStorm.
 * User: Florian
 * Date: 11/06/2015
 * Time: 11:16
 */

namespace src\limagaapp\model;
use Illuminate\Database\Eloquent\Model;

class Produit extends Model {

    protected $table = 'produit';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function e_billet(){
        return $this->hasMany('E_billet');
    }

    public function e_abonnement(){
        return $this->hasMany('E_abonnement');
    }

    public function e_abonnementfamille(){
        return $this->hasMany('E_abonnementFamille');
    }

    public function e_billetfamille(){
        return $this->hasMany('E_billetFamille');
    }




}

