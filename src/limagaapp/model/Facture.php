<?php
/**
 * Created by PhpStorm.
 * User: Florian
 * Date: 11/06/2015
 * Time: 15:00
 */
namespace src\limagaapp\model;
use Illuminate\Database\Eloquent\Model;

class Facture extends Model{

    protected $table = 'facture';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function client(){
        return $this->belongsTo('Client');
    }





}
