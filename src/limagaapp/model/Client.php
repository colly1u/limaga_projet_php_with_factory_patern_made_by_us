<?php
/**
 * Created by PhpStorm.
 * User: Florian
 * Date: 09/06/2015
 * Time: 13:42
 */
namespace src\limagaapp\model;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{

    protected $table = 'client';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function utilisateur(){
        return $this->hasOne('Utilisateur');
    }

    public function membrefamille(){
        return $this->hasMany('MembreFamille');
    }

    public function e_billet (){
        return $this->hasMany('E_billet');
    }

    public function e_abonnement(){
        return $this->hasOne('E_abonnement');
    }

    public function e_billetfamille (){
        return $this->hasMany('E_billetFamille');
    }

    public function e_abonnementfamille(){
        return $this->hasOne('E_abonnementFamille');
    }


    public function panier(){
        return $this->hasMany('Panier');

    }

    public function facture(){
        return $this->hasMany('Facture');

    }





}

