<?php
/**
 * Created by PhpStorm.
 * User: Florian
 * Date: 11/06/2015
 * Time: 15:17
 */
class BonCommande extends Model
{

    protected $table = 'boncommande';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function client()
    {
        return $this->belongsTo('Client');
    }

}


