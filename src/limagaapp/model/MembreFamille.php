<?php
/**
 * Created by PhpStorm.
 * User: Florian
 * Date: 09/06/2015
 * Time: 13:52
 */


namespace src\limagaapp\model;
use Illuminate\Database\Eloquent\Model;

class MembreFamille extends Model
{

    protected $table = 'membrefamille';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function client (){
        return $this->belongsTo('Client');
    }

}