<?php
/**
 * Created by PhpStorm.
 * User: Florian
 * Date: 09/06/2015
 * Time: 14:04
 */

namespace src\limagaapp\model;
use Illuminate\Database\Eloquent\Model;

class Ebillet extends Model {

    protected $table = 'e_billet';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function client (){
        return $this->belongsTo('Client');
    }

    public function Produit(){
        return $this->belongsTo('Produit');

    }


}