<?php
/**
 * Created by PhpStorm.
 * User: Florian
 * Date: 11/06/2015
 * Time: 15:15
 */
namespace src\limagaapp\model;
use Illuminate\Database\Eloquent\Model;

class EbilletFamille extends Model
{

    protected $table = 'e_billetfamille';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function client (){
        return $this->belongsTo('Client');
    }

    public function Produit(){
        return $this->belongsTo('Produit');

    }


}