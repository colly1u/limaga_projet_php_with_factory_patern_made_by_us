<?php
/**
 * Created by PhpStorm.
 * User: Florian
 * Date: 11/06/2015
 * Time: 14:48
 */

namespace src\limagaapp\model;
use Illuminate\Database\Eloquent\Model;

class Panier extends Model {

    protected $table = 'panier';
    protected $primaryKey = 'id';
    public $timestamps = false;



    public function client(){
        return $this->belongsTo('Client');
    }


}