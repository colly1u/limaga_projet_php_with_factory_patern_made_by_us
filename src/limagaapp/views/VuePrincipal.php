<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 10/06/2015
 * Time: 09:14
 */
namespace src\limagaapp\views;
use Slim\Slim;
class VuePrincipal {
    private $app;

    function  __construct(){
        if(!isset($app)){
            $this->app = Slim::getInstance();
        }

    }

    public function  header(){
       $this->app->render('header/header.php');
   }

    public function headerConnecte(){
        $this->app->render('header/headerConnecter.php');
    }

    public function login(){
        $this->header();
        $this->app->render('formulaire/login.php');
    }

    public function formInscription(){
        $this->header();
        $this->app->render('formulaire/formulaireInscription.php');
    }

    public function monCompte(){
        $this->app->render('compte.php');
    }

    public function magasin(){
        $this->app->render('magasin.php');
    }

    public function homePage(){
        $this->app->render('homePage.php');
    }

    public function formbillet(){
        $this->app->render('formulaire/formBillet.php');
    }

    public function formBilletFamille(){
        $this->app->render('formulaire/formBilletFamille.php');

    }

    public function formAbonnement(){
        $this->app->render('formulaire/formAbonnement.php');

    }

    public function formAbonnementFamille(){
        $this->app->render('formulaire/formAbonnementFamille.php');
    }

    public  function  formMembre(){
        $this->app->render('formulaire/formMembre.php');
    }


}