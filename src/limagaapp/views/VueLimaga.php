<?php
/**
 * Created by PhpStorm.
 * User: Florian
 * Date: 11/06/2015
 * Time: 10:55
 */
namespace src\limagaapp\views;
use src\limagaapp\control\LimagaController;
use src\limagaapp\model\Ebillet;


class VueLimaga {

    private $objet;

    public function __construct($tab){
        if (count($tab)==0 ){
            $this->objet=array();
        }else{
            $this->objet=$tab;
        }

    }

    public function afficherPannier(){
        $control=new LimagaController();
        $id=-1;
        $html = '<div class="col-lg-4 col-lg-offset-1 ">
                <h1>Mon panier</h1>
                <ul class="list-group">';

        $total=0;
        $qte=0;
        $prix=0;
        if(count($this->objet)<1){

            $html=$html.'<li class="list-group-item"> Panier vide </li>';
        }else {


            foreach ($this->objet as $key => $value) {
                $html=$html.'<li class="list-group-item">';
                $id = -1;
                $idprod = -1;
                $produit = '';

                foreach ($value as $attri => $valeur) {
                    if ($attri == 'prix') {
                        $html = $html . '<span class="badge">' . number_format($valeur,2,',',' ') . ' euros</span>';
                        $prix=$valeur;
                    }
                    if ($attri == 'qte') {
                        $html = $html . ' ' . $valeur . ' ';
                        $qte=$valeur;
                    }
                    if ($attri == 'produit_id') {
                        $idprod = $valeur;
                    }
                    if ($attri == 'e_billet_id'&& $valeur!=NULL) {
                        $id = $valeur;
                        $produit = 'e_billet';
                    }
                    if ($attri == 'e_billetfamille_id'&& $valeur!=NULL) {
                        $id = $valeur;
                        $produit = 'e_billetfamille';

                    }
                    if ($attri == 'e_abonnement_id'&& $valeur!=NULL) {
                        $id = $valeur;
                        $produit = 'e_abonnement';
                    }
                    if ($attri == 'e_abonnementfamille_id'&& $valeur!=NULL) {
                        $id = $valeur;
                        $produit = 'e_abonnementfamille';
                    }


                }
                $total=($total+($prix*$qte));

                if ($id == -1) {
                    $objet = $control->oneProduit($idprod);
                    $html = $html . ' <a href="produit/' . $objet['id'] . '"/>' . $objet['Description'] . '  </li>';

                } else {
                    switch ($produit) {
                        case 'e_abonnement':
                            $objet = $control->oneE_abonnement($id);
                            $html = $html . ' <a href="produit/e_abonnement/' . $objet['id'] . '"> e_abonnement individuelle  </a></li>';

                            break;
                        case 'e_abonnementfamille':
                            $objet = $control->oneE_abonnementFamille($id);
                            $html = $html . ' <a href="produit/e_abonnementfamille/' . $objet['id'] . '"> e_abonnement famille  </a></li>';

                            break;
                        case 'e_billet':
                            $objet = $control->oneE_billet($id);
                            $html = $html . ' <a href="produit/e_billet/' . $objet['id'] . '"> e_billet Individuelle </a></li>';

                            break;

                        case 'e_billetfamille':
                            $objet = $control->oneE_billetFamille($id);
                            $html = $html . ' <a href="produit/e_billetFamille/' . $objet['id'] . '"> e_billet famille  </a></li>';

                            break;
                    }
                }

            }


        }

        $html=$html.' <li class="list-group-item">Total: <span class="badge">'.number_format($total,2,',',' ').' euros</span></li>';
        $html = $html . '</ul>
        <form class="form-horizontal" role="form" method="post" action="factureAdd">
        <div class="form-group">
        <center><button type="submit" class="btn btn-danger">Passer ma commande</button></center>
        </div>
        </form>
        </div>';
        return($html);
    }

    public function afficherE_billet(){
        $html='<section>
	        <div class="container">
		        <div class="row">
                    <div class="col-lg-6 col-lg-offset-3">
				        <center><p><h3>Complexe aquatique de LIMAGA</h3></p><br>
				        <p><h3>Billet d entree</h3></p><br></center><p>Numero de e-billet : ';
        $control=new LimagaController();
        $client=$control->oneClient($this->objet['client_id']);
        $html=$html.$this->objet['id'].'</p><br>
				<p>Nom:'.$client['nom'].'<br>
				Prenom: '.$client['prenom'].'</p><br>
				<p>Billet dentree pour le '.$this->objet['jour'].'/'.$this->objet['mois'].'/'.$this->objet['annee'];
        $html=$html.'</p><br>
				<p> Ce billet vous permet d obtenir un bracelet qui vous donnera acces a la piscine</p><br>
				<p>Vous pouvez obtenir ce bracelet en echange de ce e-billet a une caisse ou l obtenir directement a une caisse automatique en utilisant le code-barre ci-dessous</p>
				<center> code barre:'.$this->objet['codeBarre'].'</center>
			</div>
		</div>
	    </div>
        </section>';

        return($html);
    }

    public function afficherAbonnement(){
        $html='<section>
	    <div class="container">
		<div class="row">
            <div class="col-lg-6 col-lg-offset-3">
				<center><p><h3>Complexe aquatique de LIMAGA</h3></p><br>
				<p><h3>Abonnement</h3></p><br></center>
				<p>Numero d abonnement : '.$this->objet['id'].'</p><br><p>Nom: ';

        $control=new LimagaController();
        $client=$control->oneClient($this->objet['client_id']);
        $html=$html.$client['nom'].'<br><p>
				Prenom: '.$client['prenom'].'</p><br>';
        $html=$html.'<p>Vous pouvez obtenir un bracelet a '.$this->objet['nb_billet'].' entrees en echange de ce e-billet a une caisse ou
         l obtenir directement a une caisse automatique en utilisant le code-barre ci-dessous.</p><center> code barre:
         			'.$this->objet['codeBarre'].'</center>
         </div>
		</div>
	    </div>
        </section>';
        return($html);
    }

    public  function  afficherDetailsFacture(){
        $html="";
        $control =new LimagaController();


        $total =0;
        $html= $html.'<div class="container">
                          <div class="row">
                         <div class="col-lg-8 col-lg-offset-2">
                         <center><h2>Details</h2></center>
                         <table class="table table-striped table-hover ">
                            <thead>
                            <tr>
                           <th>Type Produit</th>
                           <th>Prix</th>
                           <th>quantiter</th>
                           </tr>
                           </thead>
                           <tbody>';


        foreach($this->objet as $key => $value){
            $produit = $control->oneProduit($value['produit_id']);


            $html=$html.'<tr><td>'.$produit['Description'].'</td>';


            $html=$html.'<td>'.$produit['prixTTC'].'</td>';

            $html=$html.'<td>'.$value['qte'].'</td>';

            $total =$total+($produit['prixTTC']*$value['qte']);

            if($produit['Description']== 'e_billet_id' && $produit['Description'] !=null){
                $billet = $control->oneE_billet($value['e_billet_id']);
                $date = $billet['jour'].'/'.$billet['mois'].'/'.$billet['annee'];
                $html =$html.'<td>'.$date.'</td>';

            }

            if ($produit['Description']=='e_billetfamille_id'  && $produit['Description'] !=null) {
                $billetFam = $control->oneE_billetFamille($value['e_billetfamille_id']);
                $date = $billetFam['jour'].'/'.$billetFam['mois'].'/'.$billetFam['annee'];
                $html=$html.'<td>'.$date.'</td>';
            }
            if ($produit['Description'] == 'e_abonnement_id'  && $produit['Description'] !=null) {
                $abo = $control->oneE_abonnement($value['e_abonnement_id']);
                $date = $abo['jour'].'/'.$abo['mois'].'/'.$abo['annee'];
                $html=$html.'<td>'.$date.'</td>';
            }

            if ($produit['Description'] == 'e_abonnementfamille_id' && $produit['Description'] !=null) {
                $abo = $control->oneE_abonnement($value['e_abonnement_id']);
                $date = $abo['jour'].'/'.$abo['mois'].'/'.$abo['annee'];
                $html=$html.'<td>'.$date.'</td>';
            }


        }
        $html=$html.'</tr></tbody>';
        $html=$html.'</table><center>total : '.$total.' euros</center></div></div></div>';


        return($html);

    }

    public  function  afficherFacture(){
    $html= '

                <div class="col-lg-6 col-lg-offset-3">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <center><h2 class="panel-title">Facture</h2></center>
                        </div>';
                var_dump($this->objet);
              '<p>Facture numero :'.$this->objet['facture_id'].'</p>';
                        '<p>Date :'.$this->objet['date_fact'].'</p><br>';
                        $control = new LimagaController();
                        $client = $control->findUtilisateurByIdClient($_SESSION['client_id']);
                        '<p><i>Client :</i><br><br>';
                        'Nom :'.$client->nom;
                        'Prenom :'.$client->prenom.'<br>';
                        'Adresse :'.$client->adresse.'<br>';
                        'Tel : '.$client->numTel.'</p><br><br>';

            $html = $html.'</div>
            </div>
            </form>
            </div>
            </div>';
        return($html);
    }


    public function  afficheMembre(){

        $control =new LimagaController();
        $html='<div class="container">
                          <div class="row">
                         <div class="col-lg-10 col-lg-offset-1">
                         <center><h1>Ma Famille</h1></center>
                         <table class="table table-striped table-hover ">
                            <thead>
                            <tr>
                           <th>Nom</th>
                           <th>Prenom</th>
                           <th>Age</th>
                           <th>Niveau de natation</th>
                           </tr>
                           </thead>
                           <tbody>';

        foreach($this->objet as $key => $val){
            $html=$html.'<tr>';
            foreach($val as $k => $v){
                if($k != 'id' && $k !='client_id'){
                    $html = $html.'<td>'.$v.'</td>';
                }
            }

        }
        $html=$html.'</tr></tbody>';
        $html=$html.'</table></div></div></div>';

        return($html);

    }









}