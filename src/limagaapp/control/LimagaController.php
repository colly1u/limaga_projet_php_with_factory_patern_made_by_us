<?php
/**
 * Created by PhpStorm.
 * User: Florian
 * Date: 09/06/2015
 * Time: 14:24
 */
namespace src\limagaapp\control;
use src\limagaapp\model\Client;
use src\limagaapp\model\EabonnementFamille;
use src\limagaapp\model\Ebillet;
use src\limagaapp\model\Eabonnement;
use src\limagaapp\model\EbilletFamille;
use src\limagaapp\model\Facture;
use src\limagaapp\model\MembreFamille;
use src\limagaapp\model\Utilisateur;
use src\limagaapp\model\Produit;
use src\limagaapp\model\Panier;
use Slim\Slim;

class LimagaController {

    public function __construct(){

    }
    //Client
    public function oneClient($id){
        $cli=Client::find($id);
        return($cli->toArray());
    }

    public function allClient(){
        $all=Client::all();
        return($all->toArray());
    }

    //Utilisateur
    public function oneUtilisateur($id){
        $user=Utilisateur::find($id);
        return($user->toArray());
    }

    public function allUtilisateur(){
        $all=Utilisateur::all();
        return($all->toArray());
    }

    public function findUtilisateurByIdClient($id){
        $user=Utilisateur::where('client_id', '=', $id)->get();
        return($user->toArray());
    }

    public function findUtilisateurByPseudo($name){
        $user=Utilisateur::where('pseudo', '=', $name)->get();
        return($user->toArray());


    }

    public function verifierConnexion(){
        $post=Slim::getInstance()->request->post();
        $control=new LimagaController();
        if((!isset($post['valider'])|| ($post['valider']!='ok'))){
            echo('Vous n avez pas remplie le formulaire <br>');
        }else{
            $pseudo=$control->findUtilisateurByPseudo($post['inputPseudo']);
            if(count($pseudo)==1 && password_verify($post['inputPassword'],$pseudo[0]['pass'])) {
                $_SESSION['pseudo'] = $post['inputPseudo'];
                $_SESSION['pass'] = $post['inputPassword'];
                $_SESSION['ip'] = $_SERVER['REMOTE_ADDR'];
                $_SESSION['client_id'] = $pseudo[0]['client_id'];
                $_SESSION['panier'] = array();
            }
        }
    }

    public function deconnexion(){
        session_unset();
        session_destroy();
    }

    //famille
    public function oneMembreFamille($id){
        $fami=MembreFamille::find($id);
        return($fami->toArray());
    }

    public function allMembreFamille(){
        $all=MembreFamille::all();
        return($all->toArray());
    }

    public function findMembreByIdClient($id){
        $fami=MembreFamille::where('client_id', '=', $id)->get();
        return($fami->toArray());
    }

    //e_billet
    public function oneE_billet($id){
        $billet=Ebillet::find($id);
        return($billet->toArray());
    }

    public function allE_billet(){
        $all=Ebillet::all();
        return($all->toArray());
    }

    public function findE_billetByIdClient($id){
        $billet=Ebillet::where('client_id', '=', $id)->get();
        return($billet->toArray());
    }

    //e-billet famille
    public function oneE_billetFamille($id){
        $billet=EbilletFamille::find($id);
        return($billet->toArray());
    }

    public function e_billetFamilleByIdClient($id){
        $billet=EbilletFamille::where('client_id', '=', $id)->get();
        return($billet->toArray());
    }


    //e_abonnement
    public function oneE_abonnement($id){
        $abo=Eabonnement::find($id);
        return($abo->toArray());
    }

    public function allE_abonnement(){
        $all=Eabonnement::all();
        return($all->toArray());
    }

    public function findE_abonnementByIdClient($id){
        $abo=Ebillet::where('client_id', '=', $id)->get();
        return($abo->toArray());
    }

    //e_abonnement famille
    public function oneE_abonnementFamille($id){
        $abo=EabonnementFamille::find($id);
        return($abo->toArray());
    }



    //Produit

    public function oneProduit($id){
        $cli=Produit::find($id);
        return($cli->toArray());

    }

    //Panier

    public function onePanier($id){
        $panier=Panier::find($id);
        return($panier->toArray());
    }

    public function deconnexionMajPanierOneClient($id){
        $panierCli=Panier::whereRaw('client_id = ? and actif=1',[$id])->get();
        foreach($panierCli as $index=>$objPanier){
            $panier=Panier::find($objPanier['id']);
            $panier->actif=0;
            $panier->save();

        }

    }

    public function allProduitPanierClient($id){
        $panier=Panier::whereRaw('client_id = ? and actif=1',[$id])->get();
        return($panier->toArray());

    }

    public function allProduitPanierByIdFacture($id){
        $panier=Panier::where('facture_id', '=', $id)->get();
        return($panier->toArray());

    }


    //facture
    public  function  findFacture($id){
        $fact = Facture::find($id);
       if($fact != null){
           return($fact->toArray());
       }
    }







}