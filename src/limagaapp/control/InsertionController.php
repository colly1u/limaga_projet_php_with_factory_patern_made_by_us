<?php
/**
 * Created by PhpStorm.
 * User: Florian
 * Date: 09/06/2015
 * Time: 15:14
 */
namespace src\limagaapp\control;
use src\limagaapp\model\Client;
use src\limagaapp\model\Eabonnement;
use src\limagaapp\model\Ebillet;
use src\limagaapp\model\EbilletFamille;
use src\limagaapp\model\Facture;
use src\limagaapp\model\MembreFamille;
use src\limagaapp\model\Panier;
use src\limagaapp\model\Utilisateur;
use src\limagaapp\model\EabonnementFamille;
use src\limagaapp\control\LimagaController;
use Slim\Slim;
class InsertionController {



    public function ajouterClient(){

        $post=Slim::getInstance()->request->post();
        if((!isset($post['valider'])|| ($post['valider']!='ok'))){
            echo('Formulaire vide <br>');


        }elseif (!filter_var($post['inputAge'],FILTER_VALIDATE_INT)){
            echo('l age rentre est incorecte <br>');

        }elseif (!filter_var($post['inputEmail'],FILTER_VALIDATE_EMAIL )) {

            echo('l email rentre est incorecte <br>');

        }elseif(!filter_var($post['inputDateD'],FILTER_VALIDATE_INT)||!filter_var($post['inputDateM'],FILTER_VALIDATE_INT)||!filter_var($post['inputDateY'],FILTER_VALIDATE_INT)){
            echo('la date de naissance doit etre compose de chiffres uniquement et ne doit pas commencer par "0" ');

        }elseif($post['inputDateD']<1 || $post['inputDateD']>31){
            echo('Votre jour de naissance n existe pas <br>');

        }elseif($post['inputDateM']<1 || $post['inputDateM']>12){
            echo('Votre mois de naissance n existe pas <br>');

        }elseif($post['inputAge']<12 ||$post['inputDateY']<idate('Y',date('YYYY'))-12 || $post['inputDateY']<idate('Y',date('YYYY'))-130){
            echo('annee de Naissance invalide ou/et age inferieur a 12 ans <br>');

        }else{
            $control=new LimagaController();
            $res=$control->findUtilisateurByPseudo(filter_var($post['inputPseudo'],FILTER_SANITIZE_STRING));
            if(count($res==0)) {
                $client = new Client();
                $client->nom = filter_var($post['inputNom'], FILTER_SANITIZE_STRING);
                $client->prenom = filter_var($post['inputPrenom'], FILTER_SANITIZE_STRING);
                $client->adresse = filter_var($post['inputAdresse'], FILTER_SANITIZE_STRING);
                $date = '';
                if ($post['inputDateD'] > 9) {
                    $date = $post['inputDateD'] . '/';
                } else {
                    $date = '0' . $post['inputDateD'] . '/';
                }
                if ($post['inputDateM'] > 9) {
                    $date = $date . $post['inputDateM'] . '/';
                } else {
                    $date = $date . '0' . $post['inputDateM'] . '/';
                }
                $date = $date . $post['inputDateY'];
                $client->dateNais = filter_var($date, FILTER_SANITIZE_STRING);
                $client->age = filter_var($post['inputAge'], FILTER_SANITIZE_NUMBER_INT);
                $client->lvl_natation = filter_var($post['selectLvlNatation'], FILTER_SANITIZE_STRING);
                $client->numTel = filter_var($post['inputTel'], FILTER_SANITIZE_STRING);
                $client->save();

                $user=new Utilisateur();
                $user->pseudo=filter_var($post['inputPseudo'],FILTER_SANITIZE_STRING);
                $alea=array('cost'=> 12);
                $hash=password_hash($post['inputPassword'], PASSWORD_DEFAULT ,$alea);
                $user->pass=$hash;
                $user->alea=$alea['cost'];
                $user->date_enregistrement = date('Y-m-d',time());
                $user->IP_enregistrement=$_SERVER['REMOTE_ADDR'];
                $user->email=filter_var($post['inputEmail'],FILTER_SANITIZE_EMAIL);
                $user->client_id=$client->id;
                $user->save();
            }
        }
    }

    public  function ajouterE_billet(){
        $control=new LimagaController();


        if((!isset($_POST['valider'])|| ($_POST['valider']!='ok'))){

        }else{
            if($_POST['inputDateD']<1 || $_POST['inputDateD']>31){
                echo('Votre jour de naissance n existe pas <br>');

            }elseif($_POST['inputDateM']<1 || $_POST['inputDateM']>12){
                echo('Votre mois de naissance n existe pas <br>');

            }elseif($_POST['inputDateY']<idate('Y',date('YYYY'))){
                echo('il est trop tard'.$_POST['inpuDateY'].'<br>');
            }elseif(!filter_var($_POST['inputDateD'],FILTER_VALIDATE_INT)||!filter_var($_POST['inputDateM'],FILTER_VALIDATE_INT)
                ||!filter_var($_POST['inputDateY'],FILTER_VALIDATE_INT)){

                echo('la date de reservation doit etre compose de chiffres uniquement et ne doit pas commencer par "0" ');

            }else{

                $billet = new Ebillet();
                $billet->client_id = $_SESSION['client_id'];
                $billet->produit_id= 1;
                $billet->jour = $_POST['inputDateD'];
                $billet->mois = $_POST['inputDateM'];
                $billet->annee = $_POST['inputDateY'];
                $billet->codeBarre= $billet->id.$_SESSION['client_id'].'1'.$_POST['inputDateD'].$_POST['inputDateM'].$_POST['inputDateY'];
                $billet->save();

                $tab = $control->oneProduit(1);
                $panier = new Panier();
                $panier->client_id = $_SESSION['client_id'];
                $panier->actif = 1;
                $panier->e_billet_id = $billet->id;
                $panier->produit_id= 1;
                $panier->qte = 1;
                $panier->prix = $panier->qte*$tab['prixTTC'];
                $panier->save();


            }
        }



    }

    public function ajouterE_billetFamille(){
        $control=new LimagaController();


        if((!isset($_POST['valider'])|| ($_POST['valider']!='ok'))){


        }else{
            if($_POST['inputDateD']<1 || $_POST['inputDateD']>31){
                echo('Votre jour de naissance n existe pas <br>');

            }elseif($_POST['inputDateM']<1 || $_POST['inputDateM']>12){
                echo('Votre mois de naissance n existe pas <br>');

            }elseif($_POST['inputDateY']<idate('Y',date('YYYY'))){
                echo('il est trop tard'.$_POST['inpuDateY'].'<br>');
            }elseif(!filter_var($_POST['inputDateD'],FILTER_VALIDATE_INT)||!filter_var($_POST['inputDateM'],FILTER_VALIDATE_INT)
                ||!filter_var($_POST['inputDateY'],FILTER_VALIDATE_INT)){

                echo('la date de reservation doit etre compose de chiffres uniquement et ne doit pas commencer par "0" ');

            }else{

                $billet = new EbilletFamille();
                $billet->client_id = $_SESSION['client_id'];
                $billet->produit_id= 2;
                $billet->jour = $_POST['inputDateD'];
                $billet->mois = $_POST['inputDateM'];
                $billet->annee = $_POST['inputDateY'];
                $billet->codeBarre= $billet->id.$_SESSION['client_id'].'1'.$_POST['inputDateD'].$_POST['inputDateM'].$_POST['inputDateY'];
                $billet->save();

                $tab = $control->oneProduit(2);
                $panier = new Panier();
                $panier->client_id = $_SESSION['client_id'];
                $panier->actif = 1;
                $panier->e_billetFamille_id = $billet->id;
                $panier->produit_id= 2;
                $panier->qte = 1;
                $panier->prix = $panier->qte*$tab['prixTTC'];
                $panier->save();


            }
        }




    }

    public function ajouterE_abonnement(){

        $control=new LimagaController();


        if((!isset($_POST['valider'])|| ($_POST['valider']!='ok' || !filter_var($_POST['inputEntree'],FILTER_VALIDATE_INT)))){


        }else{

            $abo = new Eabonnement();
            $abo->client_id = $_SESSION['client_id'];
            $abo->produit_id= 5;
            $abo->nb_billet=$_POST['inputEntree'];
            $abo->codeBarre= $abo->id.$_SESSION['client_id'].'1';
            $abo->save();

            $tab = $control->oneProduit(5);
            $panier = new Panier();
            $panier->client_id = $_SESSION['client_id'];
            $panier->actif = 1;
            $panier->e_abonnement_id = $abo->id;
            $panier->produit_id= 5;
            $panier->qte = 1;
            $panier->prix = $abo->nb_billet*$tab['prixTTC'];
            $panier->save();


        }

    }

    public function ajouterE_abonnementFamille(){
        $control=new LimagaController();


        if((!isset($_POST['valider'])|| ($_POST['valider']!='ok' || !filter_var($_POST['inputEntree'],FILTER_VALIDATE_INT)))){


        }else{

            $abo = new EabonnementFamille();
            $abo->client_id = $_SESSION['client_id'];
            $abo->produit_id= 4;
            $abo->nb_billet=$_POST['inputEntree'];
            $abo->codeBarre= $abo->id.$_SESSION['client_id'].'1';
            $abo->save();

            $tab = $control->oneProduit(4);
            $panier = new Panier();
            $panier->client_id = $_SESSION['client_id'];
            $panier->actif = 1;
            $panier->e_abonnementfamille_id = $abo->id;
            $panier->produit_id= 4;
            $panier->qte = 1;
            $panier->prix = $abo->nb_billet*$tab['prixTTC'];
            $panier->save();


        }

    }

    public  function  ajouterFacture(){
        $control = new LimagaController();
        $panier = $control->allProduitPanierClient($_SERVER['client_id']);
        $total=0;

        foreach($panier as $key => $val){
            foreach($val as $k => $v ){

                if($k == 'qte'){
                    $qte =$v;
                }
                if($k == 'prix'){
                    $prix =$qte*$v;
                    $total=$total+$prix;
                }
            }

        }
        $fact = new Facture();
        $fact->client_id= filter_var($_SERVER['client_id'],FILTER_SANITIZE_NUMBER_INT);
        $fact->$total =filter_var($total, FILTER_SANITIZE_NUMBER_INT);

        $fact->save();
        foreach($panier as $key=>$val){
            $panier1 = $control->onePanier($val['id']);
            $panier1->facture_id = $fact->id;
            $panier1->actif='0';
            $panier1->save();

        }
    }


    public function ajouterMembre(){
        $control=new LimagaController();


        if((!isset($_POST['valider'])|| ($_POST['valider']!='ok'))){


        }else{

            $membre = new MembreFamille();
            $membre->nom = filter_var($_POST['inputNom'], FILTER_SANITIZE_STRING);
            $membre->age =filter_var($_POST['inputAge'], FILTER_SANITIZE_NUMBER_INT);
            $membre->prenom = filter_var($_POST['inputPrenom'], FILTER_SANITIZE_STRING);
            $membre->lvl_natation = filter_var($_POST['selectLvlNatation'], FILTER_SANITIZE_STRING);
            $membre->client_id = filter_var($_SESSION['client_id'], FILTER_SANITIZE_NUMBER_INT);
            $membre->save();
        }

    }






}