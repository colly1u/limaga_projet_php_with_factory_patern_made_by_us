<?php
/**
 * Created by PhpStorm.
 * User: Florian
 * Date: 11/06/2015
 * Time: 09:01
 */
namespace src\limagaapp\control;
use Slim\Slim;
use src\limagaapp\control\InsertionController;
use src\limagaapp\views\VueLimaga;
use src\limagaapp\views\VuePrincipal;
use src\limagaapp\control\LimagaController;
class AdminController {

    private $vue,$controlInsert,$controlLimaga;

    public function __construct(){
        $this->vue=new VuePrincipal();
        $this->controlInsert=new InsertionController();
        $this->controlLimaga=new LimagaController();


    }
    public function render($name){

    }


    public function login (){
        if(isset($_SESSION['pseudo'])){
            $this->vue->headerConnecte();
            $this->vue->monCompte();
            $_SESSION['panier']=$this->controlLimaga->allProduitPanierClient($_SESSION['client_id']);
        }else{
            $this->vue->login();
        }

    }

    public function verifLog(){
        if(isset($_SESSION['pseudo'])){
            $this->vue->headerConnecte();
            $this->vue->monCompte();
        }else{
            $this->controlLimaga->verifierConnexion();
            if(isset($_SESSION['pseudo'])){
                $_SESSION['panier']=$this->controlLimaga->allProduitPanierClient($_SESSION['client_id']);
                $this->vue->headerConnecte();
                $this->vue->monCompte();

            }else{
                $this->vue->homePage();
                echo('connexion echoue');


            }
        }

    }

    public function deconnexion(){
        $this->controlLimaga->deconnexionMajPanierOneClient($_SESSION['client_id']);
        $this->controlLimaga->deconnexion();
        $this->vue->homePage();
    }


    public function formInscription(){
        if(isset($_SESSION['pseudo'])){
            $this->vue->headerConnecte();
            $this->vue->monCompte();

        }else{
            $this->vue->formInscription();
        }

    }

    public function home(){
        if(isset($_SESSION['pseudo'])){
            $_SESSION['panier']=$this->controlLimaga->allProduitPanierClient($_SESSION['client_id']);
            $this->vue->headerConnecte();
            $this->vue->monCompte();
        }else{
            $this->vue->homePage();
        }

    }

    public function magasin(){
        if(isset($_SESSION['pseudo'])){
            $_SESSION['panier']=$this->controlLimaga->allProduitPanierClient($_SESSION['client_id']);
            $this->vue->headerConnecte();
            $this->vue->magasin();
        }else{
            $this->vue->login();
        }

    }

    public function formInscriptionValid(){
        try {
            $this->vue->homePage();
            $this->controlInsert->ajouterClient();
            if (!isset($_POST['inputPseudo'])){
                throw new \Exception('inscription echoue');
            }
            $verif = $this->controlLimaga->findUtilisateurByPseudo($_POST['inputPseudo']);

            echo('inscription reussi <br>');

        }catch (\Exception $e ){
            echo( $e->getMessage().' <br>');

        }

    }

    public function afficherProduit($name,$id){
        $control=new LimagaController();
        $affiche='<br><br> Le produit demande ne vous est pas attribue';

        switch($name) {
            case 'produit':
                $res=$control->oneProduit($id);

                break;
            case 'e_billet':
                $res=$control->oneE_billet($id);
                $vue=new VueLimaga($res);
                $affiche=$vue->afficherE_billet();

                break;
            case 'e_billetFamille':
                $res=$control->oneE_billetFamille($id);
                $vue=new VueLimaga($res);
                $affiche=$vue->afficherE_billet();

                break;
            case 'e_abonnement':
                $res=$control->oneE_abonnement($id);
                $vue=new VueLimaga($res);
                $affiche=$vue->afficherAbonnement();

                break;
            case 'e_abonnementfamille':
                $res=$control->oneE_abonnementFamille($id);
                $vue=new VueLimaga($res);
                $affiche=$vue->afficherAbonnement();

                break;
            case 'facture':
                $res=$control->findFacture($id);
                $vue= new VueLimaga($res);
                $affiche=$vue->afficherFacture();
                break;
            default:

                $res['client_id']=-1;

        }
        $this->vue->headerConnecte();
        if ($res['client_id']!=$_SESSION['client_id']){
            echo($affiche);

        }else{

            echo($affiche);
        }
    }



    public function formBilletValid(){
        if(isset($_SESSION['pseudo'])){
            $this->controlInsert->ajouterE_billet();
            $this->vue->headerConnecte();

            $_SESSION['panier']=$this->controlLimaga->allProduitPanierClient($_SESSION['client_id']);
            $this->vue->monCompte();

            echo('Billet ajoute');
        }else{
            $this->vue->homePage();
        }



    }

    public function formBilletFamilleValid(){
        if(isset($_SESSION['pseudo'])){
            $this->controlInsert->ajouterE_billetFamille();
            $this->vue->headerConnecte();

            $_SESSION['panier']=$this->controlLimaga->allProduitPanierClient($_SESSION['client_id']);
            $this->vue->monCompte();

            echo('Billet ajoute');
        }else{
            $this->vue->homePage();
        }



    }

    public function formAbonnementValid(){
        if(isset($_SESSION['pseudo'])){
            $this->controlInsert->ajouterE_abonnement();
            $this->vue->headerConnecte();

            $_SESSION['panier']=$this->controlLimaga->allProduitPanierClient($_SESSION['client_id']);
            $this->vue->monCompte();

            echo('abonnement ajoute');
        }else{
            $this->vue->homePage();
        }



    }

    public function formAbonnementFamilleValid(){
        if(isset($_SESSION['pseudo'])){
            $this->controlInsert->ajouterE_abonnementFamille();
            $this->vue->headerConnecte();

            $_SESSION['panier']=$this->controlLimaga->allProduitPanierClient($_SESSION['client_id']);
            $this->vue->monCompte();

            echo('abonnement ajoute');
        }else{
            $this->vue->homePage();
        }



    }

    public  function  afficherFacture($id){
        if(isset($_SESSION['pseudo'])){
            $this->vue->headerConnecte();
            $panier = $this->controlLimaga->allProduitPanierClient($_SESSION['client_id']);

            $vue = new VueLimaga($panier);
            echo($vue->afficherDetailsFacture());

        }else{
            $this->vue->homePage();
        }
    }

    public  function afficherFactureByid($id){
        $fact = $this->controlLimaga->allProduitPanierByIdFacture($id);
        $vuefact = new VueLimaga($fact);
        echo($vuefact->afficherFacture());
    }


    public function ajouterFact(){
        if(isset($_SESSION['pseudo'])){
            $this->controlInsert->ajouterFacture();
            $this->vue->headerConnecte();
            $this->vue->monCompte();
            echo 'Commande terminer avec succes';
        }else{
           $this->vue->header();
            echo 'La commande a echoue';
        }

    }


    public  function  famille(){
        if(isset($_SESSION['pseudo'])){
            $this->vue->headerConnecte();
            $tab = $this->controlLimaga->allMembreFamille($_SESSION['client_id']);
            $vue = new VueLimaga($tab);
            $this->vue->formMembre();
            echo($vue->afficheMembre());

        }else{
            $this->vue->homePage();
        }
    }

    public  function  formFamilleValid(){
        if(isset($_SESSION['pseudo'])){
            $this->vue->headerConnecte();
            $this->controlInsert->ajouterMembre();
            $tab = $this->controlLimaga->allMembreFamille($_SESSION['client_id']);
            $vue = new VueLimaga($tab);
            echo($vue->afficheMembre());
            echo('<center>membre ajouté</center>');
        }else{
            $this->vue->homePage();

        }
    }


}