<?php
/**
 * Created by PhpStorm.
 * User: Florian
 * Date: 09/06/2015
 * Time: 09:04
 */
use src\singleton\ConnectionFactory;
use \src\limagaapp\views\VuePrincipal;
use Slim\Slim;
use \src\limagaapp\control\InsertionController;
use \src\limagaapp\control\LimagaController;
use src\limagaapp\control\AdminController;
require 'vendor/autoload.php';


$connect=ConnectionFactory::getConnection('src/conf/db.limagaapp.conf.ini');
$app = new Slim(['templates.path'=>'src/templates']);
$vue = new VuePrincipal();
$control=new AdminController();
$panier=new LimagaController();
session_start();



    $app->get('/formInscription', function() use ($control){
        $control->formInscription();
    })->name('formInscription');

    $app->post('/formInscriptionValid', function() use ($control){
        $control->formInscriptionValid();
    });

    $app->post('/testConnection',function() use ($control){
        $control->verifLog();

    })->name('homePageConnecte');



    $app->get('/login', function() use ($control){
        $control->login();
    });


    $app->get('/deconnexion', function() use ($control){
    $control->deconnexion();
    });

    $app->get('/',function() use($control){
        $control->home();
    });

    $app->get('/moncompte', function() use ($control,$panier){
        $_SESSION['panier']=$panier->allProduitPanierClient($_SESSION['client_id']);
        $control->home();
    });

    $app->get('/magasin', function() use ($control,$panier){
        $_SESSION['panier']=$panier->allProduitPanierClient($_SESSION['client_id']);
        $control->magasin();
    });

    $app->get('/produit/:name/:id', function($name,$id) use($control) {
        $control->afficherProduit($name,$id);

    })->name('produit');

    $app->get('/famille',function()use($control){
        $control->famille();

    });
    $app->get('/facture', function()use($control){
    $control->afficherFacture($_SESSION['client_id']);
    });

    $app->post('/formbilletValid',function() use($control){
        $control->formBilletValid();
    });

    $app->post('/formbilletFamilleValid',function() use($control){
        $control->formBilletFamilleValid();
    });

    $app->post('/formAbonnementValid',function() use($control){
        $control->formAbonnementValid();
    });

    $app->post('/formAbonnementFamilleValid',function() use($control){
        $control->formAbonnementFamilleValid();
    });

    $app->get('/factureAdd',function() use($control){
        $control->ajouterFact();
    });
    $app->post('/formMembreValid',function()use($control) {
        $control->formFamilleValid();
    });

$app->run();
$app->render('footer.php');
